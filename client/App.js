import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import WS from 'react-native-websocket';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  sendButton(socket) {
    this.ws.send('Hello');
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>luggit</Text>
        <Button
          onPress = {() => this.sendButton(this.socket)}
          title = "Test Send"
         />
         <WS
           ref={ref => {this.ws = ref}}
           // Change the IP Address to point to server
           url="http://172.20.10.4:8080/"
           onOpen={() => {
             console.log('Open!')
             this.ws.send('Hello')
           }}
           onMessage={console.log}
           onError={console.log}
           onClose={console.log}
           reconnect // Will try to reconnect onClose
         />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
